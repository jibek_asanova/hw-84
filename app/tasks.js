const express = require('express');
const Task = require("../models/Task");
const auth = require("../middleware/auth");

const router = express.Router();

router.post('/', auth, async (req, res) => {

  if(!req.body.title || !req.body.status) {
    return res.status(400).send('Data not valid');
  }

  const taskData = {
    user: req.user._id,
    title: req.body.title,
    description: req.body.description || null,
    status: req.body.status
  };


  const task = new Task (taskData);
  try {
    await task.save();
    res.send(task);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get('/', auth, async (req, res) => {
  try {
    const tasks = await Task.find({user: req.user._id}).populate('user', 'title description');
    res.send(tasks);
  } catch (e) {
    res.sendStatus(500)
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    const task = await Task.findById(req.params.id);

    if(req.user._id.toString() === task.user._id.toString()) {
      const task = await Task.findByIdAndDelete(req.params.id);

      if(task) {
        res.send(`task ${task.title} removed`);
      } else {
        res.status(404).send({error: 'Task not found'})
      }
    } else {
      res.status(403).send({error: 'cannot delete task'})
    }
  } catch (e) {
    res.status(500).send(e);
  }
});

router.put('/:id', auth, async (req, res) => {

  try {
    const taskData = await Task.findById(req.params.id);

    if(req.user._id.toString() === taskData.user._id.toString()) {

      const id = req.params.id;

      const taskData = {
        title: req.body.title,
        description: req.body.description || null,
        status: req.body.status
      };

      if(req.body.user) {
        return res.status(403).send({error: 'cannot update user'});
      }

      await Task.findByIdAndUpdate(id, taskData);
      const task1 = await Task.findById(id);
      res.send(task1);
    } else {
      res.status(403).send({error: 'cannot update task'})
    }
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;