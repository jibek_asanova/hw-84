const express = require('express');
const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
  if (!req.body.username || !req.body.password) {
    return res.status(400).send('Data not valid');
  }

  const userData = {
    username: req.body.username,
    password: req.body.password,
  };

  const user = new User(userData);

  try {
    user.generateToken();
    await user.save();
    res.send(user);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.post('/sessions', async (req, res) => {
  try {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
      return res.status(401).send({error: 'Username not found'});
    }
    const isMatch = await user.checkPasswords(req.body.password);

    if (!isMatch) {
      return res.status(401).send({error: 'Password is wrong'});
    }

    user.generateToken();
    await user.save();

    res.send({message: 'Username and password correct!', user});
  } catch (e) {
    res.sendStatus(500).send(e);
  }
});

module.exports = router;
