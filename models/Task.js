const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const TaskSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  status: {
    type: String,
    enum: ['new', 'in_progress', 'complete'],
    required: true,
  }
});


TaskSchema.plugin(idvalidator);

const Task = mongoose.model('Task', TaskSchema);

module.exports = Task;